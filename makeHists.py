import numpy as np
import matplotlib.pyplot as plt
from loadData import *
import preprocessing as prp

nbins = 100

fig = plt.figure(1)
ax = []
for i in range(ncols-1):
    ax.append(fig.add_subplot(6,2,i+1))
    data = train[:,i+1]
    locs0 = classes[0].copy()
    locs1 = classes[1].copy()
    data, locs0, locs1 = prp.removeNaN(data, locs0, locs1)
    data = prp.saturate_outliers(data, 1)
    print(names[i+1])
    print(i)
    print( (data.min(), data.max()) )
    print( (data[locs0].min(), data[locs0].max()) )
    print( (data[locs1].min(), data[locs1].max()) )
    print('----')
    if i+1 not in [2, 4, 5, 8, 10]:
        ax[-1].hist(data[locs0], color='C0', alpha=0.3, normed=True, bins=nbins)
        ax[-1].hist(data[locs1], color='C1', alpha=0.3, normed=True, bins=nbins)
    elif i+1 in [2]:
        bins = np.concatenate( (np.linspace(0,1.3,nbins), np.array([1.3,100000]) ))
        ax[-1].hist(data[locs0], color='C0', alpha=0.3, normed=True, bins=bins)
        ax[-1].hist(data[locs1], color='C1', alpha=0.3, normed=True, bins=bins)
        ax[-1].set_xlim( (-0.5,1.5) )
    elif i+1 in [5]:
        ax[-1].hist(data[locs0], color='C0', alpha=0.3, normed=True, bins=nbins, log=True)
        ax[-1].hist(data[locs1], color='C1', alpha=0.3, normed=True, bins=nbins, log=True)
    elif i+1 in [4, 8, 10]:
        ax[-1].hist(data[locs0], color='C0', alpha=0.3, normed=True, bins=np.linspace(0,100,nbins), log=True)
        ax[-1].hist(data[locs1], color='C1', alpha=0.3, normed=True, bins=np.linspace(0,100,nbins), log=True)
    else:
        print('ERROR ----------------')
    ax[-1].set_title( names[i+1] )

fig.subplots_adjust(left=0.050, bottom=0.070, right=0.970, top=0.930, hspace=0.950, wspace=0.100)
plt.show()
