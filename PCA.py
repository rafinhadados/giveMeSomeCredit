import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from loadData import *
import preprocessing as prp

print(trainS.shape)

pca = PCA()
PCAout = pca.fit_transform(trainS)

C = pca.components_
print( names[ 2+C[0].argmax() ] )
print( names[ 2+C[1].argmax() ] )
print( names[ 2+C[2].argmax() ] )

plt.figure(1)
plt.semilogy(pca.explained_variance_, 'bo')
plt.semilogy(pca.explained_variance_, 'b--', lw=0.5)

plt.figure(2)
ax1 = plt.subplot(221)
plt.plot(PCAout[:,0][locs0],PCAout[:,1][locs0], 'C0.', ms=1)
plt.plot(PCAout[:,0][locs1],PCAout[:,1][locs1], 'C1.', ms=1, alpha=0.3)
ax2 = plt.subplot(222, sharey=ax1)
plt.plot(PCAout[:,2][locs0],PCAout[:,1][locs0], 'C0.', ms=1)
plt.plot(PCAout[:,2][locs1],PCAout[:,1][locs1], 'C1.', ms=1, alpha=0.3)
ax3 = plt.subplot(223, sharex=ax1)
plt.plot(PCAout[:,0][locs0],PCAout[:,2][locs0], 'C0.', ms=1)
plt.plot(PCAout[:,0][locs1],PCAout[:,2][locs1], 'C1.', ms=1, alpha=0.3)
plt.show()
