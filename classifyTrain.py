import sys

import numpy as np
from loadData import *

from sklearn.model_selection import cross_val_score

from sklearn.ensemble import AdaBoostClassifier, \
                            GradientBoostingClassifier, \
                            RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB

params = {}
params['AdaBoost'] =        [ np.array([200,300,400,500,600,700]), np.array([1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0]) ]
params['GaussianNB'] =      [ np.array([1]) ]
params['GradientBoost'] =   [ np.array([0.01, 0.05, 0.1, 0.5, 1.0]) ]
params['KNeighbors'] =      [ np.array([1,3,5,7,9,11]) ]
params['RandomForest_g'] =  [ np.array([10,100,500,1000]) ]
params['RandomForest_e'] =  [ np.array([10,100,500,1000]) ]
#params['SVM'] =             [ np.arange(-3,4,1), np.arange(-3,4,1) ]

classis = {}
classis['AdaBoost'] = lambda x,y: AdaBoostClassifier(n_estimators=x, learning_rate=y)
classis['GaussianNB'] = lambda x: GaussianNB()
classis['GradientBoost'] = lambda x: GradientBoostingClassifier(loss='deviance', learning_rate=x, n_estimators=300, max_depth=3, criterion='friedman_mse')
classis['KNeighbors'] = lambda x: KNeighborsClassifier(n_neighbors=x, weights='distance', algorithm='auto', leaf_size=30, p=2, metric='minkowski')
classis['RandomForest_g'] = lambda x: RandomForestClassifier(n_estimators=x, criterion='gini')
classis['RandomForest_e'] = lambda x: RandomForestClassifier(n_estimators=x, criterion='entropy')
#classis['SVM'] = lambda x,y: SVC(kernel='rbf', C=10.**x, gamma=10.**y, cache_size=1000, decision_function_shape='ovr')


def apply_classifiers(classis, params):
    for k in params.keys():
        print('model: %s'%(k))
        if len(params[k]) == 1:
            best = -1
            bestx = -1
            for x in params[k][0]:
                sys.stdout.write('\t\t%.3f\r'%(x))
                sys.stdout.flush()
                C = classis[k](x)
                scores = cross_val_score(C, trainS, train[:,1], cv=5, n_jobs=-1, scoring='roc_auc')
                if scores.mean() > best:
                    best = scores.mean()
                    bestx = x
            print('\n%20s\t%.3f\t%.3f'%(k, bestx, best))
        elif len(params[k]) == 2:
            best = -1
            bestx = -1
            besty = -1
            for x in params[k][0]:
                for y in params[k][1]:
                    sys.stdout.write('\t\t%.3f\t%.3f\r'%(x,y))
                    sys.stdout.flush()
                    C = classis[k](x,y)
                    scores = cross_val_score(C, trainS, train[:,1], cv=5, n_jobs=-1, scoring='roc_auc')
                    if scores.mean() > best:
                        best = scores.mean()
                        bestx = x
                        besty = y
            print('\n%20s\t%.3f\t%.3f\t%.3f'%(k, bestx, besty, best))

Y = train[:,1]
print('prior: (%d) %.3f\t(%d) %.3f'%(0, (Y==0).sum()/Y.size, 1, (Y==1).sum()/Y.size))
apply_classifiers(classis,params)
