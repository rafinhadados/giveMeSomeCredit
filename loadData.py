import preprocessing as prp


names, train_raw = prp.getData('cs-training.csv')
satCol, train = prp.saturateNaN(train_raw, mode={11:'max', 6:'min'})
ncols = len(names)
names2, test_raw = prp.getData('cs-test.csv')
test = prp.saturateNaN(test_raw, mode={11:'max', 6:'min'}, sats=satCol)

classes = prp.getClasses(train[:,1])

# Saturate outliers
trainNO = train.copy()
outCol = []
for n in range(2, train.shape[1]):
    outs, trainNO[:,n] = prp.saturate_outliers(train[:,n], perc=1)
    outCol.append( outs )
testNO = test.copy()
for n in range(2, test.shape[1]):
    testNO[:,n] = prp.saturate_outliers(test[:,n], perc=1, outs=outCol[n-2])

from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
trainS = scaler.fit_transform(trainNO[:,2:])
testS = scaler.transform(testNO[:,2:])
