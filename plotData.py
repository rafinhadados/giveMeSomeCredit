import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import preprocessing as prp
from loadData import *

ax = []

fig1 = plt.figure(1)
ax.append(fig1.add_subplot(611))
ax.append(fig1.add_subplot(612,sharex=ax[0]))
ax.append(fig1.add_subplot(613,sharex=ax[0]))
ax.append(fig1.add_subplot(614,sharex=ax[0]))
ax.append(fig1.add_subplot(615,sharex=ax[0]))
ax.append(fig1.add_subplot(616,sharex=ax[0]))
fig2 = plt.figure(2)
ax.append(fig2.add_subplot(611,sharex=ax[0]))
ax.append(fig2.add_subplot(612,sharex=ax[0]))
ax.append(fig2.add_subplot(613,sharex=ax[0]))
ax.append(fig2.add_subplot(614,sharex=ax[0]))
ax.append(fig2.add_subplot(615,sharex=ax[0]))
ax.append(fig2.add_subplot(616,sharex=ax[0]))

ax[0].plot(train[:,0])
ax[0].set_title( names[0] )

locs = train[:,1] == 0
tam0 = (locs).sum()
tamt = train[:,1].size
for i in range(1,ncols):
    ax[i].plot(np.arange(0,tam0), train[:,i][locs], 'C0.', ms=1)
    ax[i].plot(np.arange(tam0,tamt), train[:,i][~locs], 'C1.', ms=1)
    ax[i].set_title( names[i] )


fig1.subplots_adjust(top=0.950, bottom=0.050, left=0.050, right=0.970, hspace=0.900, wspace=0.200)
fig2.subplots_adjust(top=0.950, bottom=0.050, left=0.050, right=0.970, hspace=0.900, wspace=0.200)
plt.show()
