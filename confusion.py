import numpy as np
import matplotlib.pyplot as plt
from loadData import *

from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix

from sklearn.ensemble import AdaBoostClassifier, \
                                GradientBoostingClassifier, \
                                RandomForestClassifier

adaC = AdaBoostClassifier(n_estimators=200, learning_rate=0.75)
gbC = GradientBoostingClassifier(loss='deviance', learning_rate=0.05, n_estimators=600, max_depth=3, criterion='friedman_mse')
rfC = RandomForestClassifier(n_estimators=1000, criterion='gini')

print('ada')
ada_yPred = cross_val_predict(adaC, trainS, y=train[:,1], cv=5, n_jobs=-1)
print('gradient')
gb_yPred = cross_val_predict(gbC, trainS, y=train[:,1], cv=5, n_jobs=-1)
print('forest')
rf_yPred = cross_val_predict(rfC, trainS, y=train[:,1], cv=5, n_jobs=-1)

adaConf = confusion_matrix(train[:,1], ada_yPred)
gbConf = confusion_matrix(train[:,1], gb_yPred)
rfConf = confusion_matrix(train[:,1], rf_yPred)

print('ada')
print(adaConf)
adaConf = adaConf.astype(np.float) / adaConf.sum(axis=1)[:, np.newaxis]
print(adaConf)

print('gb')
print(gbConf)
gbConf = gbConf.astype(np.float) / gbConf.sum(axis=1)[:,np.newaxis]
print(gbConf)

print('forest')
print(rfConf)
rfConf = rfConf.astype(np.float) / rfConf.sum(axis=1)[:,np.newaxis]
print(rfConf)

plt.figure(figsize=(9,9))
plt.pcolormesh(adaConf, vmin=0, vmax=1)
plt.colorbar()
plt.title('adaBoost')

plt.figure(figsize=(9,9))
plt.pcolormesh(gbConf, vmin=0, vmax=1)
plt.colorbar()
plt.title('grandientBoost')

plt.figure(figsize=(9,9))
plt.pcolormesh(rfConf, vmin=0, vmax=1)
plt.colorbar()
plt.title('randomForest')

plt.show()
