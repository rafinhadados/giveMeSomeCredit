from loadData import *
import numpy as np

print('------------------- TRAIN --------------------\n')
l1,c1 = np.where( np.isnan(train_raw) )
for val in list(set(c1.tolist())):
    n = names[val]
    count = (c1 == val).sum()
    print('%2d\t%20s\t%7d'%(val,n,count))

print('------------------- TEST --------------------\n')
l2,c2 = np.where( np.isnan(test_raw) )
for val in list(set(c2.tolist())):
    n = names[val]
    count = (c2 == val).sum()
    print('%2d\t%20s\t%7d'%(val,n,count))
