import numpy as np
import matplotlib.pyplot as plt
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis as QDA
from sklearn.preprocessing import minmax_scale
from loadData import *
import preprocessing as prp

# NaN removal
trainNNaN, locs0, locs1 = prp.removeNaNLines(train, classes[0], classes[1])

# Outlier removal
trainNO = np.zeros( trainNNaN.shape )
trainNO[:,0] = trainNNaN[:,0]
trainNO[:,1] = trainNNaN[:,1]
for n in range(2, trainNNaN.shape[1]):
    trainNO[:,n] = prp.saturate_outliers(trainNNaN[:,n], perc=1)

Xscaled = minmax_scale(trainNO[:,2:], feature_range=(0,1))

qda = QDA(priors=[(trainNO[:,1]==0).sum(), (trainNO[:,1]==1).sum()])
QDAout = qda.fit(Xscaled, trainNO[:,1])



#plt.plot(QDAout[ trainNO[:,1]==0, 0], QDAout[ trainNO[:,1]==1, 0])
