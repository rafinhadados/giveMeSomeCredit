import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from loadData import *
import preprocessing as prp

# NaN removal
trainNNaN, locs0, locs1 = prp.removeNaNLines(train, classes[0], classes[1])

# Outlier removal
trainNO = np.zeros( trainNNaN.shape )
trainNO[:,0] = trainNNaN[:,0]
trainNO[:,1] = trainNNaN[:,1]
for n in range(2, trainNNaN.shape[1]):
    trainNO[:,n] = prp.saturate_outliers(trainNNaN[:,n], perc=1)

print(trainNO.shape)

scaler = StandardScaler()
trainNOs = scaler.fit_transform(trainNO[:,2:])

X_data, X_none, y_data, y_none = train_test_split(trainNOs, trainNO[:,1], test_size=(1-0.10))

print(X_data.shape)
print(y_data.size)
print((1.*(y_data==1).sum()) / y_data.size)
print((1.*(y_data==0).sum()) / y_data.size)

locs0 = y_data == 0
locs1 = y_data == 1

tsne = TSNE(n_components=3, perplexity=30, early_exaggeration=4., learning_rate=50., init='random', verbose=2)
TSNEout = tsne.fit_transform(X_data, y_data)

plt.figure(2)
ax1 = plt.subplot(221)
plt.plot(TSNEout[:,0][locs0],TSNEout[:,1][locs0], 'C0.', ms=1)
plt.plot(TSNEout[:,0][locs1],TSNEout[:,1][locs1], 'C1.', ms=1, alpha=0.3)
ax2 = plt.subplot(222, sharey=ax1)
plt.plot(TSNEout[:,2][locs0],TSNEout[:,1][locs0], 'C0.', ms=1)
plt.plot(TSNEout[:,2][locs1],TSNEout[:,1][locs1], 'C1.', ms=1, alpha=0.3)
ax3 = plt.subplot(223, sharex=ax1)
plt.plot(TSNEout[:,0][locs0],TSNEout[:,2][locs0], 'C0.', ms=1)
plt.plot(TSNEout[:,0][locs1],TSNEout[:,2][locs1], 'C1.', ms=1, alpha=0.3)
plt.show()
