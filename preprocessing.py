import numpy as np
import pandas as pd

def getData(name):
    train = pd.read_csv(name)

    names = []
    for n,k in enumerate(train.columns):
        names.append( k )

    return names, train.values

def getClasses(classCol):
    locs1 = classCol == 0
    locs2 = classCol == 1
    classes = [locs1, locs2]
    return classes

def saturateNaN(dataMat, modedef='min', mode={}, sats=None):
    output = dataMat.copy()
    l,c = np.where(np.isnan(dataMat))
    cols = list(set(c.tolist()))

    # SeriousDlqin2yrs
    if 1 in cols:
        cols.remove(1)

    saturationCol = np.zeros(len(cols))

    for n,c in enumerate(cols):
        col = dataMat[:,c]
        locs, = np.where( np.isnan(col) )
        col = col[ ~np.isnan(col) ]
        if c not in mode.keys():
            modeloc = modedef
        else:
            modeloc = mode[c]
        if modeloc == 'min' and sats is None:
            m = col.min()
            r = np.percentile(col,1)
            if r == 0:
                r = 1
            output[locs,c] = m - r
            saturationCol[n] = m - r
        elif modeloc == 'max' and sats is None:
            m = col.max()
            r = np.percentile(col,1)
            if r == 0:
                r = 1
            output[locs,c] = m + r
            saturationCol[n] = m + r
        elif sats is not None:
            if sats[n] == 0:
                print('error satCol %s %s'%(repr(sats), repr(sats[n])))
            output[locs,c] = sats[n]
    if sats is None:
        return saturationCol, output
    else:
        return output

def removeNaN(data, locs0, locs1):
    nnanidx, = np.where(np.logical_not(np.isnan(data)))
    data = data[nnanidx]
    locs0 = locs0[nnanidx]
    locs1 = locs1[nnanidx]
    return data, locs0, locs1

def removeNaNLines(data, locs0, locs1):
    idxLines = np.isnan(data).sum(axis=1) == 0
    return data[idxLines,:], locs0[idxLines], locs1[idxLines]

def remove_outliers(data, locs0, locs1, perc=1):
    minval, maxval = np.percentile(data, [perc, 100-perc])
    bool_arr = np.logical_and(data>minval, data<maxval)
    if bool_arr.sum() < 0.5*data.size:
        return data, locs0, locs1
    else:
        return data[ np.logical_and(data>minval, data<maxval) ], \
                locs0[ np.logical_and(data>minval, data<maxval) ], \
                locs1[ np.logical_and(data>minval, data<maxval) ], \

def saturate_outliers(data, perc=1, outs=None):
    if outs is None:
        minval, maxval = np.percentile(data, [perc, 100-perc])
    else:
        minval = outs[0]
        maxval = outs[1]
    bool_arr = np.logical_and(data>minval, data<maxval)
    data[ data<=minval ] = minval
    data[ data>=maxval ] = maxval
    if outs is None:
        return [minval, maxval], data
    else:
        return data
